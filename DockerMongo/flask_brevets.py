"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, render_template, request, session
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import os
import logging
from json import loads
from pprint import pprint #FIXME !!!!!!!!!!!!!!!!!!!!!!

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
data = []

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/submit", methods=['POST'])
def submit():
    
    controls = request.form.copy()
    controls = controls.popitem()[0] # turn str into array
    controls = loads(controls)

    db.tododb.delete_one({})  # clear database
    db.tododb.insert_one(controls)
 
    return flask.render_template('calc.html')
    
@app.route("/display", methods=['POST'])
def display():
    items = db.tododb.find_one()
    ret = [items['distance']]

    for control in items['controls']:
        ret.append(list(items['controls'][control].values()))

    print(ret)
    
    return flask.render_template('results.html', items=ret)

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('b_dist', 999, type=int)
    brevet_start_time = request.args.get('b_st', 999, type=str)
    brevet_start_date = request.args.get('b_sd', 999, type=str)
    
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    
    start = brevet_start_date + " " + brevet_start_time
    start = arrow.get(start, "YYYY-MM-DD HH:mm")
    
    open_time = acp_times.open_time(km, brevet_dist_km, start.isoformat())
    close_time = acp_times.close_time(km, brevet_dist_km, start.isoformat())
    
    open_time = open_time.format("YYYY-MM-DD HH:mm")
    close_time = close_time.format("YYYY-MM-DD HH:mm")
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
