# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database. Takes input uising jQuery, then processes the information using ajax flask. Output is reflect immediately using ajax.

Added "Submit" and "Display" buttons. When "Submit" is clicked, the control times currently being displayed are entered into a MongoDB database. Before they are added, the database is cleared - it will only hold controls for one brevet at a time. When "Display" is clicked, whatever is presently in the database is displayed on a fresh page with only the results on it. 

At least one control must be entered - otherwise, an error message will be shown. Control times are calculated using the logic described here (https://rusa.org/pages/acp-brevet-control-times-calculator). It is assumed that all distances entered are valid control points, so the last one is not >+/-20% from the brevet distance. Per rusa.org, small discrepancies between the final control point and the brevet distance do not affect the open/close times.

The close time for the starting control (0km) is 1 hour after the start time. See paragraph 1 of "oddities" in the link above.

## Button Test Cases

**Submit**

* No control times entered -> Error message shown beneath button
* Control times are entered -> Add to Mongo database, no output shown
* Pressed twice -> "Writes over" previously stored data with current data

**Display**

* No control times entered, first use of "Display" *ever* -> Presents empty table with labels
* All other states -> Presents formatted table with whatever relevant information is currently being held in the database (even if nothing was submitted in the current session)

## Contact

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com

